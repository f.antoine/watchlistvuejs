import Vue from 'vue'
import Router from 'vue-router'
import Cine from '@/components/Cine'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Cine',
      component: Cine
    }
  ]
})
